package dev.joseluis.biblioteca_joseluis_sb.service;

import dev.joseluis.biblioteca_joseluis_sb.model.Usuario;
import dev.joseluis.biblioteca_joseluis_sb.repository.UsuarioRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class UsuarioServiceTest {

    @Autowired
    private UsuarioRepository repository;

    private Usuario newTestInstance(){
        Usuario usuario = new Usuario();
        usuario.setNombre("test");
        usuario.setApellido1("test");
        usuario.setApellido2("test");
        usuario.setDni("test");
        usuario.setDomicilio("test");
        usuario.setPoblacion("test");
        usuario.setProvincia("test");
        usuario.setFechanacimiento(LocalDate.now());
        return usuario;
    }
    @Test
    public void addUsuario(){
        Usuario usuario = newTestInstance();
        Usuario saved = repository.save(usuario);
        assertEquals(saved.getNombre(), usuario.getNombre());
    }

}
