package dev.joseluis.biblioteca_joseluis_sb.service;

import dev.joseluis.biblioteca_joseluis_sb.model.Libro;
import dev.joseluis.biblioteca_joseluis_sb.repository.LibroRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class LibroServiceTest {

    @Autowired
    private LibroRepository repository;

    private Libro newTestInstance(){
        Libro libro = new Libro();
        libro.setTitulo("testing");
        libro.setEditorial("testing");
        libro.setAutor("testing");
        libro.setGenero("testing");
        libro.setPaisautor("testing");
        libro.setNumeropaginas(400);
        libro.setAnoedicion("2021");
        libro.setCantidad(100);

        return libro;
    }
    @Test
    public void addLibro(){
        Libro libro = newTestInstance();
        Libro saved = repository.save(libro);
        assertEquals(saved.getTitulo(), libro.getTitulo());
    }

}
