package dev.joseluis.biblioteca_joseluis_sb.service;

import dev.joseluis.biblioteca_joseluis_sb.model.Libro;
import dev.joseluis.biblioteca_joseluis_sb.model.Prestamo;
import dev.joseluis.biblioteca_joseluis_sb.model.Usuario;
import dev.joseluis.biblioteca_joseluis_sb.repository.LibroRepository;
import dev.joseluis.biblioteca_joseluis_sb.repository.PrestamoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PrestamoServiceTest {

    @Autowired
    private PrestamoRepository repository;

    private Prestamo newTestInstance(){
        Prestamo prestamo = new Prestamo();
        Libro libro = new Libro();
        libro.setId(1);
        prestamo.setLibrocodigo(libro);

        Usuario usuario = new Usuario();
        usuario.setId(1);
        prestamo.setUsuariocodigo(usuario);
        prestamo.setFechaprestamo(LocalDate.now());
        prestamo.setFechamaximadevolucion(LocalDate.now());
        return prestamo;
    }
    @Test
    public void addLibro(){
        Prestamo prestamo = newTestInstance();
        Prestamo saved = repository.save(prestamo);
        assertEquals(saved.getLibrocodigo(), prestamo.getLibrocodigo());
    }

}
