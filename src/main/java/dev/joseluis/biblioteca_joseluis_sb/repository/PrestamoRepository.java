package dev.joseluis.biblioteca_joseluis_sb.repository;

import dev.joseluis.biblioteca_joseluis_sb.model.Prestamo;
import dev.joseluis.biblioteca_joseluis_sb.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrestamoRepository extends JpaRepository<Prestamo, Integer> {

    @Query(
            value = "SELECT numeropedido, librocodigo, usuariocodigo, usuarios.nombre, libros.titulo FROM prestamos JOIN usuarios ON prestamos.usuariocodigo=usuarios.codigousuario AND prestamos.fechadevolucion IS NOT NULL JOIN libros ON prestamos.librocodigo=libros.codigolibro",
            nativeQuery = true)
    List<Prestamo> getPrestamosSinPenalizaciones();

    @Query(
            value = "SELECT numeropedido, librocodigo, usuariocodigo, usuarios.nombre, libros.titulo FROM prestamos JOIN usuarios ON prestamos.usuariocodigo=usuarios.codigousuario AND prestamos.fechadevolucion IS NULL JOIN libros ON prestamos.librocodigo=libros.codigolibro",
            nativeQuery = true)
    List<Prestamo> getPrestamosConPenalizaciones();
}
