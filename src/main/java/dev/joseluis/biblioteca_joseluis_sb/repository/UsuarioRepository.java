package dev.joseluis.biblioteca_joseluis_sb.repository;

import dev.joseluis.biblioteca_joseluis_sb.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    @Query(
            value = "SELECT * FROM usuarios WHERE penalizaciones > 0",
            nativeQuery = true)
    List<Usuario> getUsuariosConPenalizaciones();

    @Query(
            value = "SELECT * FROM usuarios WHERE penalizaciones <= 0",
            nativeQuery = true)
    List<Usuario> getUsuariosSinPenalizaciones();
}
