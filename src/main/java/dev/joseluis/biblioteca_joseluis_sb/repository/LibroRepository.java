package dev.joseluis.biblioteca_joseluis_sb.repository;

import dev.joseluis.biblioteca_joseluis_sb.model.Libro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LibroRepository extends JpaRepository<Libro, Integer> {
}
