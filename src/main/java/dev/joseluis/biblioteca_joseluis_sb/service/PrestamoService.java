package dev.joseluis.biblioteca_joseluis_sb.service;

import dev.joseluis.biblioteca_joseluis_sb.model.Prestamo;
import dev.joseluis.biblioteca_joseluis_sb.model.Usuario;
import dev.joseluis.biblioteca_joseluis_sb.repository.PrestamoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrestamoService {

    @Autowired
    PrestamoRepository repository;


    public void addPrestamo(Prestamo prestamo) {
        repository.save(prestamo);
    }

    public Prestamo getPrestamo(int id){
        return repository.getById(id);
    }

    public List<Prestamo> getPrestamos() {
        return repository.findAll();
    }

    public List<Prestamo> getPrestamosSinPenalizaciones() {
        return repository.getPrestamosSinPenalizaciones();
    }

    public List<Prestamo> getPrestamosConPenalizaciones() {
        return repository.getPrestamosConPenalizaciones();
    }
}
