package dev.joseluis.biblioteca_joseluis_sb.service;

import dev.joseluis.biblioteca_joseluis_sb.model.Usuario;
import dev.joseluis.biblioteca_joseluis_sb.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository repository;

    public void addUsuario(Usuario usuario) {
        repository.save(usuario);
    }

    public void modifyUsuario(Usuario usuario){
        repository.save(usuario);
    }

    public void removeUsuario(Usuario usuario) {
        repository.deleteById(usuario.getId());
    }

    public List<Usuario> getUsuarios() {
        return repository.findAll();
    }

    public Object getUsuariosSinPenalizaciones() {
        return repository.getUsuariosSinPenalizaciones();
    }

    public Object getUsuariosConPenalizaciones() {
        return repository.getUsuariosConPenalizaciones();
    }
}
