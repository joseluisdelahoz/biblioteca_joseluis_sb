package dev.joseluis.biblioteca_joseluis_sb.service;

import dev.joseluis.biblioteca_joseluis_sb.exception.RecordNotFoundException;
import dev.joseluis.biblioteca_joseluis_sb.model.Libro;
import dev.joseluis.biblioteca_joseluis_sb.repository.LibroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Optional;

@Service
public class LibroService {

    @Autowired
    LibroRepository repository;

    public Libro getLibro(int id) throws RecordNotFoundException {
        Optional<Libro> libro = repository.findById(id);
        if(libro.isPresent()){
            return libro.get();
        }else{
            throw new RecordNotFoundException("No student record exist for given id ", id);
        }
    }

    public void addLibro(Libro libro){
        repository.save(libro);
    }

    public void modifyLibro(Libro libro) throws RecordNotFoundException {
        repository.save(libro);
    }

    public void removeLibro(int id){
        repository.deleteById(id);
    }

    public List<Libro> getLibros(){
        return repository.findAll();
    }
}
