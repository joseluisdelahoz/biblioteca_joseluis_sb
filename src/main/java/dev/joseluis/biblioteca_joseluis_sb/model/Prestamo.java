package dev.joseluis.biblioteca_joseluis_sb.model;

import javax.persistence.*;
import java.time.LocalDate;

@Table(name = "prestamos", indexes = {
        @Index(name = "librocodigo", columnList = "librocodigo"),
        @Index(name = "usuariocodigo", columnList = "usuariocodigo")
})
@Entity
public class Prestamo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "numeropedido", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "librocodigo")
    private Libro librocodigo;

    @ManyToOne
    @JoinColumn(name = "usuariocodigo")
    private Usuario usuariocodigo;

    @Column(name = "fechaprestamo", nullable = false)
    private LocalDate fechaprestamo;

    @Transient
    private String fechaPres;

    public String getFechaPres() {
        return fechaPres;
    }

    public void setFechaPres(String fechaPres) {
        this.fechaPres = fechaPres;
    }

    public String getFechaLimite() {
        return fechaLimite;
    }

    public void setFechaLimite(String fechaLimite) {
        this.fechaLimite = fechaLimite;
    }

    @Column(name = "fechamaximadevolucion")
    private LocalDate fechamaximadevolucion;

    @Transient
    private String fechaLimite;

    @Column(name = "fechadevolucion")
    private LocalDate fechadevolucion;

    public LocalDate getFechadevolucion() {
        return fechadevolucion;
    }

    public void setFechadevolucion(LocalDate fechadevolucion) {
        this.fechadevolucion = fechadevolucion;
    }

    public LocalDate getFechamaximadevolucion() {
        return fechamaximadevolucion;
    }

    public void setFechamaximadevolucion(LocalDate fechamaximadevolucion) {
        this.fechamaximadevolucion = fechamaximadevolucion;
    }

    public LocalDate getFechaprestamo() {
        return fechaprestamo;
    }

    public void setFechaprestamo(LocalDate fechaprestamo) {
        this.fechaprestamo = fechaprestamo;
    }

    public Usuario getUsuariocodigo() {
        return usuariocodigo;
    }

    public void setUsuariocodigo(Usuario usuariocodigo) {
        this.usuariocodigo = usuariocodigo;
    }

    public Libro getLibrocodigo() {
        return librocodigo;
    }

    public void setLibrocodigo(Libro librocodigo) {
        this.librocodigo = librocodigo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}