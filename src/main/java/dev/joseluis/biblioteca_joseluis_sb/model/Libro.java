package dev.joseluis.biblioteca_joseluis_sb.model;

import javax.persistence.*;

@Table(name = "libros")
@Entity
public class Libro {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codigolibro", nullable = false)
    private Integer id;

    @Column(name = "titulo", nullable = false, length = 60)
    private String titulo;

    @Column(name = "editorial", length = 25)
    private String editorial;

    @Column(name = "autor", nullable = false, length = 25)
    private String autor;

    @Column(name = "genero", nullable = false, length = 20)
    private String genero;

    @Column(name = "paisautor", length = 20)
    private String paisautor;

    @Column(name = "numeropaginas", nullable = false)
    private Integer numeropaginas;

    @Column(name = "anoedicion", length = 5)
    private String anoedicion;

    @Column(name = "cantidad")
    private Integer cantidad;

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getAnoedicion() {
        return anoedicion;
    }

    public void setAnoedicion(String anoedicion) {
        this.anoedicion = anoedicion;
    }

    public Integer getNumeropaginas() {
        return numeropaginas;
    }

    public void setNumeropaginas(Integer numeropaginas) {
        this.numeropaginas = numeropaginas;
    }

    public String getPaisautor() {
        return paisautor;
    }

    public void setPaisautor(String paisautor) {
        this.paisautor = paisautor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Libro{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", editorial='" + editorial + '\'' +
                ", autor='" + autor + '\'' +
                ", genero='" + genero + '\'' +
                ", paisautor='" + paisautor + '\'' +
                ", numeropaginas=" + numeropaginas +
                ", anoedicion='" + anoedicion + '\'' +
                ", cantidad=" + cantidad +
                '}';
    }
}