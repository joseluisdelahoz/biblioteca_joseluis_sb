package dev.joseluis.biblioteca_joseluis_sb.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Table(name = "usuarios")
@Entity
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codigousuario", nullable = false)
    private Integer id;

    @Column(name = "nombre", nullable = false, length = 15)
    private String nombre;

    @Column(name = "apellido1", nullable = false, length = 20)
    private String apellido1;

    @Column(name = "apellido2", length = 20)
    private String apellido2;

    @Column(name = "dni", length = 12)
    private String dni;

    @Column(name = "domicilio", nullable = false, length = 50)
    private String domicilio;

    @Column(name = "poblacion", nullable = false, length = 30)
    private String poblacion;

    @Column(name = "provincia", length = 20)
    private String provincia;

    @Column(name = "fechanacimiento", nullable = false)
    private LocalDate fechanacimiento;

    @Transient
    private String fecha;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Column(name = "penalizaciones")
    private Integer penalizaciones;

    public Integer getPenalizaciones() {
        return penalizaciones;
    }

    public void setPenalizaciones(Integer penalizaciones) {
        this.penalizaciones = penalizaciones;
    }

    public LocalDate getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(LocalDate fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", dni='" + dni + '\'' +
                ", domicilio='" + domicilio + '\'' +
                ", poblacion='" + poblacion + '\'' +
                ", provincia='" + provincia + '\'' +
                ", fechanacimiento=" + fechanacimiento +
                ", fecha='" + fecha + '\'' +
                ", penalizaciones=" + penalizaciones +
                '}';
    }

}