package dev.joseluis.biblioteca_joseluis_sb.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class RecordNotFoundException extends Throwable {

    private String exceptionDetail;
    private Object fieldValue;

    public RecordNotFoundException(String exceptionDetail, int fieldValue) {
        super(exceptionDetail + " - " + fieldValue);
        this.exceptionDetail = exceptionDetail;
        this.fieldValue = fieldValue;
    }
}
