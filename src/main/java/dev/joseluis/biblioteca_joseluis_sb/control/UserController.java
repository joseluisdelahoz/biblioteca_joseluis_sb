package dev.joseluis.biblioteca_joseluis_sb.control;

import dev.joseluis.biblioteca_joseluis_sb.model.Libro;
import dev.joseluis.biblioteca_joseluis_sb.model.Prestamo;
import dev.joseluis.biblioteca_joseluis_sb.model.Usuario;
import dev.joseluis.biblioteca_joseluis_sb.service.LibroService;
import dev.joseluis.biblioteca_joseluis_sb.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;

@Controller
public class UserController {

    @Autowired
    UsuarioService service;

    @RequestMapping("/user/")
    public String getIndexPage(@ModelAttribute("usuario") Usuario usuario) {
        return "user/menu";
    }

    @RequestMapping("/user/add")
    public String getAddUsuarioPage(@ModelAttribute("usuario") Usuario usuario){
        return "user/add";
    }

    @PostMapping("/user/add")
    public String addUsuario(@ModelAttribute("usuario") Usuario usuario){
        System.out.println(usuario.getFecha() );
        LocalDate birth = LocalDate.parse(usuario.getFecha());
        usuario.setFechanacimiento(birth);
        usuario.setPenalizaciones(0);
        service.addUsuario(usuario);
        return "user/add";
    }

    @RequestMapping("/user/modify")
    public String getModifiyUsuarioPage(@ModelAttribute("usuario") Usuario usuario){
        return "user/modify";
    }

    @PostMapping("/user/modify")
    public String modifyUsuario(@ModelAttribute("usuario") Usuario usuario){
        System.out.println(usuario.getFecha() );
        LocalDate birth = LocalDate.parse(usuario.getFecha());
        usuario.setFechanacimiento(birth);
        service.modifyUsuario(usuario);
        return "user/modify";
    }

    @RequestMapping("/user/remove")
    public String getRemoveUsuarioPage(@ModelAttribute("usuario") Usuario usuario){
        return "user/remove";
    }

    @PostMapping("/user/remove")
    public String removeUsuario(@ModelAttribute("usuario") Usuario usuario){
        service.removeUsuario(usuario);
        return "user/remove";
    }

    @RequestMapping("/user/list")
    public ModelAndView listUsuario(@ModelAttribute("usuario") Usuario usuario){
        HashMap<String, Object> params = new HashMap<>();
        params.put("lista_usuarios", service.getUsuarios());
        params.put("lista_usuarios_sin_penalizaciones", service.getUsuariosSinPenalizaciones());
        params.put("lista_usuarios_con_penalizaciones", service.getUsuariosConPenalizaciones());
        return new ModelAndView("user/list", params);
    }
}
