package dev.joseluis.biblioteca_joseluis_sb.control;

import dev.joseluis.biblioteca_joseluis_sb.model.Libro;
import dev.joseluis.biblioteca_joseluis_sb.model.Prestamo;
import dev.joseluis.biblioteca_joseluis_sb.model.Usuario;
import dev.joseluis.biblioteca_joseluis_sb.service.PrestamoService;
import dev.joseluis.biblioteca_joseluis_sb.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.util.HashMap;

@Controller
public class PrestamoController {

    @Autowired
    PrestamoService service;

    @RequestMapping("/loan/")
    public String getIndexPage(@ModelAttribute("prestamo") Prestamo prestamo) {
        return "loan/menu";
    }

    @RequestMapping("/loan/add")
    public String getAddPrestamoPage(@ModelAttribute("prestamo") Prestamo prestamo){
        return "loan/add";
    }

    @PostMapping("/loan/add")
    public String addPrestamo(@ModelAttribute("prestamo") Prestamo prestamo){
        LocalDate pre = LocalDate.parse(prestamo.getFechaPres());
        prestamo.setFechaprestamo(pre);
        LocalDate limite = LocalDate.parse(prestamo.getFechaLimite());
        prestamo.setFechamaximadevolucion(limite);
        service.addPrestamo(prestamo);
        return "loan/add";
    }

    @RequestMapping("/loan/return")
    public String getModifyPrestamoPage(@ModelAttribute("prestamo") Prestamo prestamo){
        return "loan/return";
    }

    @PostMapping("/loan/return")
    public String modifyPrestamo(@ModelAttribute("prestamo") Prestamo prestamo){
        System.out.println(LocalDate.now());
        Prestamo toUpdate = service.getPrestamo(prestamo.getId());
        toUpdate.setFechadevolucion(LocalDate.now());
        service.addPrestamo(toUpdate);
        return "loan/return";
    }

    @RequestMapping("/loan/list")
    public ModelAndView getListPrestamoPage(@ModelAttribute("prestamo") Prestamo prestamo){
        HashMap<String, Object> params = new HashMap<>();
        params.put("lista_prestamos", service.getPrestamos());
        params.put("lista_prestamos_devueltos", service.getPrestamosSinPenalizaciones());
        params.put("lista_prestamos_no_devueltos", service.getPrestamosConPenalizaciones());
        return new ModelAndView("loan/list", params);
    }

}
