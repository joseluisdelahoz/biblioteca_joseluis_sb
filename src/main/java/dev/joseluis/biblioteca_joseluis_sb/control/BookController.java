package dev.joseluis.biblioteca_joseluis_sb.control;

import dev.joseluis.biblioteca_joseluis_sb.exception.RecordNotFoundException;
import dev.joseluis.biblioteca_joseluis_sb.model.Libro;
import dev.joseluis.biblioteca_joseluis_sb.service.LibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;

@Controller
public class BookController {

    @Autowired
    LibroService service;

    @RequestMapping("/book/")
    public String getIndexPage(@ModelAttribute("libro") Libro libro) {
        return "book/menu";
    }

    @RequestMapping("/book/add")
    public String getAddLibroPage(@ModelAttribute("libro") Libro libro){
        return "book/add";
    }

    @PostMapping("/book/add")
    public String addLibro(@ModelAttribute("libro") Libro libro){
        System.out.println(libro.getAutor());
        service.addLibro(libro);
        return "book/add";
    }

    @RequestMapping("/book/modify")
    public String getModifyPage(@ModelAttribute("libro") Libro libro){
        return "book/modify";
    }

    @PostMapping("/book/modify")
    public String modifyLibro(@ModelAttribute("libro") Libro libro){
        try {
            service.modifyLibro(libro);
        } catch (RecordNotFoundException e) {
            System.err.println(e.getMessage());
        }
        return "book/modify";
    }

    @RequestMapping("/book/remove")
    public String getRemovePage(@ModelAttribute("libro") Libro libro){
        return "book/remove";
    }

    @PostMapping("/book/remove")
    public String removeLibro(@ModelAttribute("libro") Libro libro){
        service.removeLibro(libro.getId());
        return "book/remove";
    }

    @RequestMapping("/book/list")
    public ModelAndView getListaLibrosPage(@ModelAttribute("libro") Libro libro){
        HashMap<String, Object> params = new HashMap<>();
        params.put("lista_libros", service.getLibros());
        return new ModelAndView("book/list", params);
    }
}
