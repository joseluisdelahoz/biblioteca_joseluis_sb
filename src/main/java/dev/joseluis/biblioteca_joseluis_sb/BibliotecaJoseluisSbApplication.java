package dev.joseluis.biblioteca_joseluis_sb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BibliotecaJoseluisSbApplication {

	public static void main(String[] args) {
		SpringApplication.run(BibliotecaJoseluisSbApplication.class, args);
	}

}
