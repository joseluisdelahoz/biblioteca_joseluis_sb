<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title></title>
</head>
<body>
<h1>Lista de libros</h1>
<table>
    <%@include file="../templates/user-nav.jsp"%>
    <h2>Todos los usuarios</h2>
    <table>
        <c:forEach items="${lista_usuarios}" var="usuario">
            <tr>
                <td>${usuario.getId()}</td>
                <td>${usuario.getNombre()}</td>
                <td>${usuario.getApellido1()}</td>
                <td>${usuario.getApellido2()}</td>
                <td>${usuario.getDni()}</td>
                <td>${usuario.getDomicilio()}</td>
                <td>${usuario.getPoblacion()}</td>
                <td>${usuario.getProvincia()}</td>
                <td>${usuario.getFechanacimiento()}</td>
                <td>${usuario.getPenalizaciones()}</td>
            </tr>
        </c:forEach>
    </table>

    <h2>Usuarios sin penalizaciones</h2>
    <table>
        <c:forEach items="${lista_usuarios_sin_penalizaciones}" var="usuario">
            <tr>
                <td>${usuario.getId()}</td>
                <td>${usuario.getNombre()}</td>
                <td>${usuario.getApellido1()}</td>
                <td>${usuario.getApellido2()}</td>
                <td>${usuario.getDni()}</td>
                <td>${usuario.getDomicilio()}</td>
                <td>${usuario.getPoblacion()}</td>
                <td>${usuario.getProvincia()}</td>
                <td>${usuario.getFechanacimiento()}</td>
                <td>${usuario.getPenalizaciones()}</td>
            </tr>
        </c:forEach>
    </table>
    <h2>Usuarios con penalizaciones</h2>
    <table>
        <c:forEach items="${lista_usuarios_con_penalizaciones}" var="usuario">
            <tr>
                <td>${usuario.getId()}</td>
                <td>${usuario.getNombre()}</td>
                <td>${usuario.getApellido1()}</td>
                <td>${usuario.getApellido2()}</td>
                <td>${usuario.getDni()}</td>
                <td>${usuario.getDomicilio()}</td>
                <td>${usuario.getPoblacion()}</td>
                <td>${usuario.getProvincia()}</td>
                <td>${usuario.getFechanacimiento()}</td>
                <td>${usuario.getPenalizaciones()}</td>
            </tr>
        </c:forEach>
    </table>
</table>
</body>
</html>
