<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
    <title>Modificar usuario</title>
</head>
<body>
<%@include file="../templates/user-nav.jsp"%>
<h1>Modificar usuario</h1>
<form:form action="${pageContext.request.contextPath}/user/modify" modelAttribute="usuario">

    <form:label path="id">ID:</form:label><br>
    <form:input path="id"/><br>
    <br>

    <form:label path="nombre">Nombre:</form:label><br>
    <form:input path="nombre"/><br>

    <form:label path="apellido1">Primer apellido:</form:label><br>
    <form:input path="apellido1"/><br>

    <form:label path="apellido2">Segundo apellido:</form:label><br>
    <form:input path="apellido2"/><br>

    <form:label path="dni">DNI:</form:label><br>
    <form:input path="dni"/><br>

    <form:label path="domicilio">Domicilio:</form:label><br>
    <form:input path="domicilio"/><br>

    <form:label path="poblacion">Población:</form:label><br>
    <form:input path="poblacion"/><br>

    <form:label path="provincia">Provincia:</form:label><br>
    <form:input path="provincia"/><br>

    <form:label path="fecha">Fecha de nacimiento:</form:label><br>
    <form:input type="date" path="fecha"/><br>

    <form:label path="penalizaciones">Penalizaciones:</form:label><br>
    <form:input path="penalizaciones"/><br>

    <br>
    <input type="submit" value="Submit">
</form:form>
</body>
</html>
