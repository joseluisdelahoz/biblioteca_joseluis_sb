<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <title>Eliminar usuario</title>
</head>
<body>
<%@include file="../templates/user-nav.jsp"%>
<h1>Eliminar usuario</h1>
<form:form action="${pageContext.request.contextPath}/user/remove" modelAttribute="usuario">
    <form:label path="id">ID:</form:label><br>
    <form:input path="id"/><br><br>

    <br>
    <input type="submit" value="Submit">
</form:form>
</body>
</html>
