<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Eliminar Libro</title>
</head>
<body>
<%@include file="../templates/book-nav.jsp"%>
<h1>Eliminar libro</h1>
<form:form action="${pageContext.request.contextPath}/book/remove" modelAttribute="libro">
    <form:label path="id">ID:</form:label><br>
    <form:input path="id"/><br>
    <br>
    <input type="submit" value="Submit">
</form:form>
</body>
</html>
