<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
    <title>Añadir Libro</title>
</head>
<body>
<%@include file="../templates/book-nav.jsp"%>
<h1>Añadir libro</h1>
<form:form action="${pageContext.request.contextPath}/book/add" modelAttribute="libro">

    <form:label path="titulo">Título:</form:label><br>
    <form:input path="titulo"/><br>

    <form:label path="editorial">Editorial:</form:label><br>
    <form:input path="editorial"/><br>

    <form:label path="autor">Autor:</form:label><br>
    <form:input path="autor"/><br>

    <form:label path="genero">Género:</form:label><br>
    <form:input path="genero"/><br>

    <form:label path="paisautor">Autor país:</form:label><br>
    <form:input path="paisautor"/><br>

    <form:label path="numeropaginas">Número de páginas:</form:label><br>
    <form:input path="numeropaginas"/><br>

    <form:label path="anoedicion">Año edición:</form:label><br>
    <form:input path="anoedicion"/><br>

    <form:label path="cantidad">Cantidad de copias:</form:label><br>
    <form:input path="cantidad"/><br>

    <br>
    <input type="submit" value="Submit">
</form:form>
</body>
</html>
