<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title></title>
</head>
<body>
<%@include file="../templates/book-nav.jsp"%>
<h1>Lista de libros</h1>
<table>
    <c:forEach items="${lista_libros}" var="libro">
        <tr>
            <td>${libro.getId()}</td>
            <td>${libro.getTitulo()}</td>
            <td>${libro.getEditorial()}</td>
            <td>${libro.getAutor()}</td>
            <td>${libro.getGenero()}</td>
            <td>${libro.getPaisautor()}</td>
            <td>${libro.getNumeropaginas()}</td>
            <td>${libro.getAnoedicion()}</td>
            <td>${libro.getCantidad()}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
