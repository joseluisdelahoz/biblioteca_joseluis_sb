<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
    <title>Añadir préstamo</title>
</head>
<body>
<%@include file="../templates/loan-nav.jsp"%>
<h1>Añadir préstamo</h1>
<form:form action="${pageContext.request.contextPath}/loan/add" modelAttribute="prestamo">

    <form:label path="librocodigo">Libro ID:</form:label><br>
    <form:input path="librocodigo"/><br>

    <form:label path="usuariocodigo">Usuario ID:</form:label><br>
    <form:input path="usuariocodigo"/><br>

    <form:label path="fechaPres">Fecha préstamo:</form:label><br>
    <form:input type="date" path="fechaPres"/><br>

    <form:label path="fechaLimite">Fecha devolución:</form:label><br>
    <form:input type="date" path="fechaLimite"/><br>

    <br>
    <input type="submit" value="Submit">
</form:form>
</body>
</html>
