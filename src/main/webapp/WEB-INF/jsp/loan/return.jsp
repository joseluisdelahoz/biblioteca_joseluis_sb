<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <title>Devolver libro</title>
</head>
<body>
<%@include file="../templates/loan-nav.jsp"%>
<h1>Devolver préstamo</h1>
<form:form action="${pageContext.request.contextPath}/loan/return" modelAttribute="prestamo">
    <form:label path="id">ID:</form:label><br>
    <form:input path="id"/><br>

    <br>
    <input type="submit" value="Submit">
</form:form>
</body>
</html>
