<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
    <title></title>
</head>
<body>
<h1>Lista de libros</h1>
<table>
    <%@include file="../templates/loan-nav.jsp"%>
    <h2>Todos los préstamos</h2>
    <table>
        <c:forEach items="${lista_prestamos}" var="prestamo">
            <tr>
                <td>${prestamo.getId()}</td>
                <td>${prestamo.getLibrocodigo()}</td>
                <td>${prestamo.getUsuariocodigo()}</td>
                <td>${prestamo.getFechaprestamo()}</td>
                <td>${prestamo.getFechamaximadevolucion()}</td>
                <td>${prestamo.getFechadevolucion()}</td>
            </tr>
        </c:forEach>
    </table>

    <h2>Préstamos devueltos</h2>
    <table>
        <c:forEach items="${lista_prestamos_devueltos}" var="prestamo">
            <tr>
                <td>${prestamo.getId()}</td>
                <td>${prestamo.getLibrocodigo()}</td>
                <td>${prestamo.getUsuariocodigo()}</td>
                <td>${prestamo.getFechaprestamo()}</td>
                <td>${prestamo.getFechamaximadevolucion()}</td>
                <td>${prestamo.getFechadevolucion()}</td>
            </tr>
        </c:forEach>
    </table>

    <h2>Préstamos sin devolver</h2>
    <table>
        <c:forEach items="${lista_prestamos_no_devueltos}" var="prestamo">
            <tr>
                <td>${prestamo.getId()}</td>
                <td>${prestamo.getLibrocodigo()}</td>
                <td>${prestamo.getUsuariocodigo()}</td>
                <td>${prestamo.getFechaprestamo()}</td>
                <td>${prestamo.getFechamaximadevolucion()}</td>
                <td>${prestamo.getFechadevolucion()}</td>
            </tr>
        </c:forEach>
    </table>
</table>
</body>
</html>
